﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectDotNetComplet.Models;

namespace ProjectDotNetComplet.Controllers
{
    public class AngularJSController : Controller
    {
        private angularJSEntities1 db = new angularJSEntities1();

        //
        // GET: /AngularJS/

        public ActionResult Index()
        {
            return View(db.Customers.ToList());
        }

        //
        // GET: /AngularJS/Details/5

        public ActionResult Details(int id = 0)
        {
            Customers customers = db.Customers.Find(id);
            if (customers == null)
            {
                return HttpNotFound();
            }
            return View(customers);
        }

        //
        // GET: /AngularJS/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /AngularJS/Create

        [HttpPost]
        public ActionResult Create(Customers customers)
        {
            if (ModelState.IsValid)
            {
                db.Customers.Add(customers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customers);
        }

        //
        // GET: /AngularJS/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Customers customers = db.Customers.Find(id);
            if (customers == null)
            {
                return HttpNotFound();
            }
            return View(customers);
        }

        //
        // POST: /AngularJS/Edit/5

        [HttpPost]
        public ActionResult Edit(Customers customers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customers);
        }

        //
        // GET: /AngularJS/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Customers customers = db.Customers.Find(id);
            if (customers == null)
            {
                return HttpNotFound();
            }
            return View(customers);
        }

        //
        // POST: /AngularJS/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Customers customers = db.Customers.Find(id);
            db.Customers.Remove(customers); 
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}